var sock = null;
var wsuri = "ws://127.0.0.1:2222/ws";
$(function () {
    $.getScript("/api/sso/myinfo?cb=cb");

})


function cb(info) {
    if (info.state == 1) {
        showLoginUi()
    } else {
        showUserInfo(info)
        $.get("/api/server/myAccount?" + new Date().getTime(), showAwardInfo);
    }
}


function showAwardInfo(data) {
    var myAccount = data.Account;
    if (!myAccount)
        return;
    var orders = data.Order;
    if (!orders.length)
        return;
    showFriendsCharts(orders, myAccount);
}


// 登陆界面
function showNewOrderUI() {
    var refId = 09;
    var htm = [];
    htm.push('<div class="newOrder" >')
    htm.push('<a class="iconfont icon-close" ></a>')
    htm.push('<h2 > 报单 </h2>')
    htm.push('<form id="login">')
    htm.push('<span ><em class="iconfont icon-icon"></em><input   type="text" placeholder="手机" name="mobile" value="18620131415" /></span>')
    htm.push('<span ><em class="iconfont icon-mima"></em><select  placeholder="金额" name="price" >')
    htm.push('<option value="3000" selected >3000 快</option>')
    htm.push('<option value="10000" >10000 快</option>')
    htm.push('<option value="20000" >20000 快</option>')
    htm.push('<option value="30000" >30000 快</option>')
    htm.push('</select></span>')
    htm.push('<span ><em class="iconfont icon-mima"></em><input   type="text" placeholder="推荐人手机" name="introducerMob"   value="000000" /></span>')
    htm.push('<a class="submit" >确定</a>')
    htm.push('</form>')
    htm.push('</div>')

    $('#dialog').html(htm.join('')).show();
    $('#dialog').find('.submit').click(newOrderFn)
    $('#dialog').find('.icon-close').click(closeDialog)
}

var MSGOBJ = {
    CalcAwarIsOk: function (msg) {
        var fn = function () {
            var btn = $("#calcAward");
            btn.html("计算奖金");
            btn.removeClass('off').addClass('on')
            btn.click(showCalcAwar)
        }
        if (msg) setTimeout(fn, 3000)
    },

    Send: function (msg) {
        sock.send(nsg);
    }
}


function showCalcAwar() {
    var url = '/api/server/calcAward'
    var result = function (res) {
        var btn = $("#calcAward");
        btn.html("计算中...");
        btn.removeClass('on').addClass('off')
        btn.removeAttr('onclick')
    }

    $.ajax({
        type: "get",
        url: url,
        success: result
    });
}

function ClickNextChildrenEvent(a, b) {
    var c = b.data;
    // c.children = a
    $.get("/api/server/myfriends?uid=" + b.data.uid, function (res) {
        c.children = res
        a.children[1].children = c
    })

}
var myChart;
function showFriendsCharts(myOrders, myAccount) {

    var fn = function (orders) {
        var aa = [];
        orders.forEach(function (a, b) {
            aa.push({name: a.Price, oid: a.Id, children: [], myAccount: myAccount, icon: "img/xin.svg"})
        });
        return aa
    }

    ztreeFriends(fn(myOrders), myAccount)


}

function ztreeFriends(dataRef, myAccount) {

    var setting2 = {

        callback: {
            onClick: onClickNode
        },
        treeNode: {
            open: true
        }
    }
    var treeObj = $.fn.zTree.init($("#trees"), setting2, dataRef);
    var myDirectOrders = []
    $.get('/api/server/myDirectOrders', function (data) {
        if (!data.length) return;
        myDirectOrders = data
    });

    function onClickNode(a, b, c) {

        $.get('/api/server/myfriends?oid=' + c.oid, function (data) {
            if (!data.length) return;

            var newData = [];
            data.forEach(function (a, b) {
                var node = treeObj.getNodeByParam("oid", a.oid, c);
                if (!node) {

                    if (a.aid == myAccount.id) {
                        a.icon = "img/xin.svg"
                    } else {
                        a.icon = "img/xinGreen.svg"
                    }
                    if (myDirectOrders.length) {
                        myDirectOrders.forEach(function (aa, bb) {
                            if (aa == a.oid) {
                                a.icon = 'img/xinOrange.svg';
                            }
                        })
                    }

                    a.name = a.oid + "--" + a.name + "--" + a.price;
                    newData.push(a)
                }
            })


            treeObj.addNodes(c, newData);
        });
    }


}

