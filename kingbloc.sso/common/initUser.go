package common

import (
	"time"

	"kingbloc.sso/user"

	"kingbloc.util/util"
)

func InitUser() {
	util.Eng.DropTables(new(user.User), new(user.State))
	util.Eng.CreateTables(new(user.User), new(user.State))
	newMangeUserTemplate(611041314, "15100000000", "123456")
}

// 管理者帐号
func newMangeUserTemplate(qq int64, mob, mm string) {
	user := new(user.User)
	user.Id = 0
	user.Username = "a-z0-9"
	user.Password = util.Md5EncodeForPassword(mm)
	user.Alias = "从无到有"
	user.Mobile = mob
	user.Alipay = mob + "@qq.com"
	user.Wechat = "heimawangzi_com"
	user.QQ = qq
	user.Email = mob + "@kingbloc.com"
	user.City = "北京"
	user.Address = "中关村055号"
	user.Sex = 1
	user.Identity = "6688"
	user.Create = time.Now()
	user.Last = time.Now()
	user.Add()
}
