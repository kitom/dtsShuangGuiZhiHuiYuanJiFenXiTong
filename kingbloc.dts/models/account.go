package model

import (
	"kingbloc.util/util"
	"time"
)

type Account struct {
	Id            int64     `json:"id"`
	SsoUserId     int64     `json:"ssoUserId"`
	Award         float64   `json:"award"` // 总奖金
	CharmAward    float64   `json:"charmAward"`
	MyOrderIds    string    `json:"myOrderIds" xorm:"varchar(9000)"` // 多个单用,分割
	Integral      int64     `json:"integral"`                        // 积分
	MultipleSales int       `json:"multipleSales"`                   // 复销分
	Balance       float64   `json:"balance"`                         // 余额
	Enable        int8      `json:"enable"`                          // 是启用0，不启用1
	Types         int8      `json:"types"`                           // 账户类型: 0普通会员，1区域代理账户，2管理员
	Password2     string    `json:"-"`                               // 交易密码
	CreateTime    time.Time `json:"createTime"`                      // 创建时间日志
}

func NewAccount() (account *Account) {

	account = new(Account)
	account.Password2 = util.Md5EncodeForPassword("000000")
	account.CreateTime = time.Now()
	account.CharmAward = 0
	return account

}

func (ac *Account) ById(id int64) *Account {
	MysqlEngine.Id(id).Get(ac)
	if ac.Id == 0 {
		return nil
	}
	return ac
}

func FindAccountBySsoId(ssoid int64) *Account {
	acc := new(Account)
	MysqlEngine.Where("sso_user_id = ?", ssoid).Get(acc)
	if acc.Id > 0 {
		return acc
	}
	return nil
}

func (account *Account) Add() {
	MysqlEngine.InsertOne(account)

}

func (account *Account) Update() {
	MysqlEngine.Id(account.Id).Update(account)
}

func (account *Account) UpdateCols(column ...string) {
	MysqlEngine.Id(account.Id).Cols(column...).Update(account)
}
