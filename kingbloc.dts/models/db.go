package model

import (
	"fmt"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"strconv"
	"time"
)

var MysqlEngine *xorm.Engine

const (
	TimeFormat = "2006-01-02 15:04:05"
)

func GetMysqlEngine(dbtype, url, dbname, pwd, showSql string) *xorm.Engine {
	if MysqlEngine != nil {
		return MysqlEngine
	}
	dbUrl := strings.Join([]string{strings.Join([]string{dbname, pwd}, ":"), url}, "@")
	// fmt.Println(dbtype + " : " + dbUrl)
	var err error
	MysqlEngine, err = xorm.NewEngine(dbtype, dbUrl)
	if err != nil {
		fmt.Printf(err.Error())
	}
	isShow, _ := strconv.ParseBool(showSql)
	if isShow {
		MysqlEngine.ShowSQL(isShow)
	} else {
		MysqlEngine.ShowSQL(false)
	}
	MysqlEngine.TZLocation, _ = time.LoadLocation("Asia/Shanghai")
	return MysqlEngine

}
