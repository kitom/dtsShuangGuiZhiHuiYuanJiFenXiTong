package tool

import ()

// 倍增数量,双轨制
const GlobalMultiply_2 = 2

// managementAward income layers
const ManagementAwardRate = 0.05
const LuckyAwardRate = 0.01
const AmountAwardRate = 0.15
const LayerAwardRate = 0.5

func GetUnitPrice() map[string]int {
	var UnitPrice = make(map[string]int)
	UnitPrice["3000"] = 10
	UnitPrice["10000"] = 15
	UnitPrice["20000"] = 17
	UnitPrice["30000"] = 21
	return UnitPrice
}

// 存在相应的等级
func ExtisPrice(price string) bool {
	var UnitPrice = make(map[string]bool)
	UnitPrice["3000"] = true
	UnitPrice["10000"] = true
	UnitPrice["20000"] = true
	UnitPrice["30000"] = true
	return UnitPrice[price]
}

func GetCharm5_9Price(price string) float64 {
	var UnitPrice = make(map[string]float64)
	UnitPrice["3000"] = 15
	UnitPrice["10000"] = 50
	UnitPrice["20000"] = 100
	UnitPrice["30000"] = 150

	return UnitPrice[price]
}
func GetCharm10Price(price string) float64 {
	var UnitPrice = make(map[string]float64)
	UnitPrice["3000"] = 30
	UnitPrice["10000"] = 100
	UnitPrice["20000"] = 200
	UnitPrice["30000"] = 300
	return UnitPrice[price]
}
